# Figma plugin API человеческим языком
### Часть 2
## Взаимодействие с Figma

[<< Часть 1 Hello World!!!](https://habr.com/ru/post/589521/)

В [статье](https://habr.com/ru/post/541412/) про написание скриптов для  Adobe After Effects я предложил читателям сделать песочницу кода, работающую прямо в AE. Давайте теперь сделаем такую же и для Figma. За основу можно взять [заготовку](https://gitlab.com/figma-plugin-examples/figma-plugin-simple) из предыдущей статьи. Здесь вы найдете необходимые для работы плагина файлы **manifest.json**, **index.html** и **plugin.js**.

Для начала откроем **manifest.json** и заменим значение поля name на актуальное. Я назвал плагин **Script Notepad**.

_manifest.json_

    {
        "name": "Script Notepad",
        "api": "1.0.0",
        "main": "plugin.js",
        "ui": "index.html",
        "editorType": [
            "figma"
        ]
    }

Теперь в **index.html** создадим текстовое поле для ввода и кнопку для запуска кода:

_index.html_
    
    <textarea rows="20" cols="43"></textarea>
    <button>Run</button>

А так же добавим им стилей:

_index.html_

    <style>
       textarea {
           padding: 10px;
           resize: none;
       }
       button {
           display: block;
           cursor: pointer;
           margin: 20px auto;
           width: 100px;
       }
    </style>

Теперь следует обработать нажатие кнопки и отправить в фигму код, который мы напишем в текстовом поле.

_index.html_

    <script>
       document.querySelector('button').onclick = () => {
           const codeText = document.querySelector('textarea').value;
           parent.postMessage({ pluginMessage: codeText}, '*');
       }
    </script>

Мы извлекаем из текстового поле содержимое и с помощью метода postMessage отправляем его в Figma. Метод **postMessage** принимает два параметра. Первый - это объект с полем **pluginMessage**, значение которого может быть любым типом данных. Значение этого поля Figma получит при обработке сообщения. Второй параметр - строка “*”, указывает на источник сообщения. Подробности работы метода можно найти [здесь](https://www.figma.com/plugin-docs/api/properties/figma-ui-postmessage/).

С **index.html** все, переходим к **plugin.js**. Тут мы первым делом открываем окно плагина.

_plugin.js_

    figma.showUI (
        __html__,
        {width: 400, height: 400}
    );

А следом обрабатываем получение сообщения из окна плагина.

_plugin.js_

    figma.ui.onmessage = e => {
        try {
            eval(e)
        } catch (err) {
            console.error(err);
        }
    }

В обработчике мы пробуем преобразовать сообщение в код, а в случае неудачи выводим сообщение об ошибке в консоль.

Вот собственно и все. Запускаем плагин, пишем и запускаем код. Результат работы:

![Альтернативный текст](./assets/img-0.png)

Консоль в Figma можно открыть в меню **Plugins -> Development -> Open console**

![Альтернативный текст](./assets/img-1.png)